--[[ LOVE STUFF ]]
function love.load()
  gameState = "menu"
  myFont = love.graphics.newFont(30)
  timer = 0

  love.window.setMode(700, 700)
  myWorld = love.physics.newWorld(0, 500, false)
  myWorld:setCallbacks(beginContact, endContact, preSolve, postSolve)

  sprites = {}
  sprites.coin_sheet = love.graphics.newImage('sprites/coin_sheet.png')
  sprites.player_jump = love.graphics.newImage('sprites/player_jump.png')
  sprites.player_stand = love.graphics.newImage('sprites/player_stand.png')

  require('player')
  require('show')
  require('coin')
  anim8 = require 'anim8'
  sti = require 'sti'
  camera = require 'camera'

  saveData = {}
  saveData.bestTime = 999

  if love.filesystem.exists("data.lua") then
      local data = love.filesystem.load("data.lua")
      data()
  end

  platforms = {}

  cam = camera()
  gameMap = sti("maps/map.lua", { "box2d" })
  for i, obj in pairs(gameMap.layers['platforms'].objects) do
    spawnPlatform(obj.x, obj.y, obj.width, obj.height)
  end

  spawnCoins()
  resetPlayerPosition()

  DIRECTION_LEFT = -1
  DIRECTION_RIGHT = 1
end

function love.update(dt)
  myWorld:update(dt)
  playerUpdate(dt)
  gameMap:update(dt)
  coinUpdate(dt)

  cam:lookAt(player.body:getX(), love.graphics.getHeight()/2)

  for i,c in ipairs(coins) do
    c.animation:update(dt)
  end

  if gameState == "game" then
    timer = timer + dt
  end

  if #coins == 0 and gameState == "game" then
    gameState = "menu"
    resetPlayerPosition()
    spawnCoins()
    if timer < saveData.bestTime then
      saveData.bestTime = math.floor(timer)
      love.filesystem.write("data.lua", table.show(saveData, "saveData"))
    end
  end
end

function love.draw()
  cam:attach()

  gameMap:drawLayer(gameMap.layers['Tile Layer 1'])

  love.graphics.draw(
      player.sprite,
      player.body:getX(),
      player.body:getY(),
      nil, player.direction, 1,
      sprites.player_stand:getWidth()/2, sprites.player_stand:getHeight()/2)

  for i, c in ipairs(coins) do
    c.animation:draw(sprites.coin_sheet, c.x, c.y, nil, nil, nil, 20.5, 21)
  end
  cam:detach()
  if gameState == "menu" then
    love.graphics.setFont(myFont)
    love.graphics.printf("Press any key to begin!",  0, 50, love.graphics.getWidth(), "center")
    love.graphics.printf("Best time:" .. saveData.bestTime,  0, 150, love.graphics.getWidth(), "center")
  end
  if gameState == "game" then
    love.graphics.print("Time:" .. math.ceil(timer), 10, 660)
  end

end

function love.keypressed(key, scancode, isrepeat)
  if key == "space" and player.grounded then
    player.body:applyLinearImpulse(0, -2800)
  end

  if gameState == "menu" then
    gameState = "game"
    timer = 0
  end
end

function spawnPlatform(x, y, width, height)
  local platform = {}
  platform.body = love.physics.newBody(myWorld, x, y, 'static')
  platform.shape = love.physics.newRectangleShape(width/2, height/2, width, height)
  platform.fixture = love.physics.newFixture(platform.body, platform.shape)
  platform.width = width
  platform.height = height

  table.insert(platforms, platform)
end


function beginContact(a, b, coll)
  player.grounded = true
end
function endContact(a, b, coll)
  player.grounded = false
end



function distanceBetweenObjects(obj1, obj2)
  return distanceBetween(obj1.x, obj1.y, obj2.x, obj2.y)
end

function distanceBetween(x1, y1, x2, y2)
  return math.sqrt((y2 - y1)^2 + (x2 - x1)^2)
end
