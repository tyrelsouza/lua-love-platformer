player = {}
player.start_x = 106
player.start_y = 465
-- Physics Engine
player.body = love.physics.newBody(myWorld, 0, 0, 'dynamic')
player.shape = love.physics.newRectangleShape(66, 92)
player.fixture = love.physics.newFixture(player.body, player.shape)
player.body:setFixedRotation(true)
--
player.sprite = sprites.player_jump
player.speed = 200
player.grounded = false
player.direction = DIRECTION_RIGHT

function playerUpdate(dt)
  if gameState == "game" then
    if love.keyboard.isDown('a') then
      player.body:setX(player.body:getX() - player.speed * dt)
      player.direction = DIRECTION_LEFT
    end

    if love.keyboard.isDown('d') then
      player.body:setX(player.body:getX() + player.speed * dt)
      player.direction = DIRECTION_RIGHT
    end

    if player.grounded == true then
      player.sprite = sprites.player_stand
    else
      player.sprite = sprites.player_jump
    end
  end
end

function resetPlayerPosition()
  player.body:setPosition(player.start_x, player.start_y)
end
